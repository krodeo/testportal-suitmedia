/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./index.html",
        "./src/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        fontFamily: {
            'SVN-Gilroy-Light': ['SVN-Gilroy Light'],
            'SVN-Gilroy-Bold': ['SVN-Gilroy Bold'],
        },
        extend: {
            colors: {
                'theme': '#003459',
                'theme-darker': '#002A48',
            },
            aspectRatio: {
                '4/3': '4 / 3',
            }
        },
    },
    plugins: [],
}

