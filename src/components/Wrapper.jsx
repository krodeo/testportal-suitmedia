export default function Wrapper({className, children}) {
    return <section className={`max-w-[1180px] mx-auto relative px-3 md:px-6 xl:px-0 ${className}`}>{children}</section>
}