import IconPresent from "../assets/icons/present.svg";

export default function CardProduct(props) {
    return (
        <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="p-2 rounded-lg shadow-lg" aria-label={props.name}>
            <div className="aspect-square overflow-hidden mb-3">
                <img src={props.image} className="object-cover w-full h-full" alt={props.name} />
            </div>
            <div className="px-2 pb-3">
                <h3 className="font-SVN-Gilroy-Bold mb-1 capitalize">{props.name}</h3>
                <div className="flex items-center gap-x-3 text-gray-500/90 text-xs mb-2">
                    <div>Product: <span className="font-SVN-Gilroy-Bold ml-1">{props.product}</span></div>
                    <div className={props.size ? '' : 'hidden'}>•</div>
                    <div className={props.size ? '' : 'hidden'}>Size: <span className="font-SVN-Gilroy-Bold ml-1">{props.size}</span></div>
                </div>
                <div className="text-sm font-SVN-Gilroy-Bold text-black/90 mb-3">{props.price} VND</div>
                <div className="flex items-center gap-x-3 px-2 py-0.5 rounded-md bg-orange-100/85">
                    <img src={IconPresent} alt="present" />
                    <span className="text-2xl">•</span>
                    <span className="text-sm font-SVN-Gilroy-Bold text-theme-darker">{props.bonus}</span>
                </div>
            </div>
        </a>
    );
}