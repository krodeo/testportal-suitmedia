export default function SubscribePanel() {
    return (
        <div className=" flex flex-col lg:flex-row lg:items-center gap-y-5 lg:gap-x-12 bg-theme p-5 md:p-7 rounded-2xl">
            <h2 className="text-xl md:text-2xl font-SVN-Gilroy-Bold text-white lg:max-w-96">Register Now So You Don't Miss Our Programs</h2>
            <div className="flex flex-col sm:flex-row md:items-center p-3 bg-white grow rounded-xl gap-y-3 gap-x-2">
                <input type="email" className="grow border border-zinc-400 py-3 px-7 text-sm rounded-lg focus:outline-none focus:border-sky-400" placeholder="Enter your Email" />
                <button className="button rounded-lg bg-theme text-white hover:bg-slate-800">Subscribe Now</button>
            </div>
        </div>
    );
} 