import { IconArrowLeft } from "../components/Icons";
import FlagVt from "../assets/icons/flag-vt.svg";
import FlagId from "../assets/icons/flag-id.svg";
import FlagUs from "../assets/icons/flag-us.svg";
import FlagJp from "../assets/icons/flag-jp.svg";
import FlagSg from "../assets/icons/flag-sg.svg";
import { useState } from "react";

export default function CurrencyPicker({ className }) {
    const [isOpen, setIsOpen] = useState(false);

    const handleClick = (e) => {
        e.preventDefault();
        setIsOpen(!isOpen);
    }

    return (
        <div className={`relative ${className}`}>
            <a href="" onClick={handleClick} className="flex items-center gap-x-2">
                <img src={FlagVt} alt="VND" />
                <div className="text-theme font-SVN-Gilroy-Bold">VND</div>
                <IconArrowLeft className="rotate-90 ml-2" />
            </a>
            <div className={`absolute mt-2 w-32 bg-white rounded-md shadow overflow-hidden py-2 ${!isOpen && 'hidden'}`}>
                <a href="#" onClick={handleClick} className="flex items-center gap-3 px-4 py-2 font-SVN-Gilroy-Bold hover:bg-gray-100"><img src={FlagId} alt="IDR" />IDR</a>
                <a href="#" onClick={handleClick} className="flex items-center gap-3 px-4 py-2 font-SVN-Gilroy-Bold hover:bg-gray-100"><img src={FlagUs} alt="USD" />USD</a>
                <a href="#" onClick={handleClick} className="flex items-center gap-3 px-4 py-2 font-SVN-Gilroy-Bold hover:bg-gray-100"><img src={FlagJp} alt="JPY" />JPY</a>
                <a href="#" onClick={handleClick} className="flex items-center gap-3 px-4 py-2 font-SVN-Gilroy-Bold hover:bg-gray-100"><img src={FlagSg} alt="SGD" />SGD</a>
            </div>
        </div>
    );
}