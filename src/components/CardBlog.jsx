export default function CardBlog(props) {
    return (
        <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="p-2 rounded-xl shadow-lg" aria-label={props.title}>
            <div className="md:aspect-4/3 overflow-hidden mb-3 rounded-lg">
                <img src={props.thumbnail} className="object-cover w-full h-full" alt={props.title} />
            </div>
            <div className="px-2 pb-3">
                <div className="inline-block px-2.5 py-0.5 leading-4 text-[10px] text-white bg-sky-500 mb-2 rounded-full">{props.category}</div>
                <h3 className="font-SVN-Gilroy-Bold mb-2 capitalize">{props.title}</h3>
                <p className="text-sm">{props.summary.length > 120 ?`${props.summary.substring(0,150)}...`:props.summary}</p>
            </div>
        </a>
    );
}