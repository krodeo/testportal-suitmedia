export default function CardPet(props) {
    return (
        <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="p-2 rounded-lg shadow-lg cursor-pointer" aria-label={`Pets ${props.name}`}>
            <div className="aspect-square overflow-hidden mb-3">
                <img src={props.image} className="object-cover w-full h-full" alt={props.name} />
            </div>
            <div className="px-2 pb-2 md:pb-4">
                <h3 className="text-sm md:text-base font-SVN-Gilroy-Bold mb-1 capitalize">{`${props.id} - ${props.name}`}</h3>
                <div className="flex flex-col md:flex-row md:items-center gap-y-1 gap-x-3 text-gray-500/90 text-xs mb-2">
                    <div>Gene: <span className="font-SVN-Gilroy-Bold ml-1">{props.gene}</span></div>
                    <div className="hidden md:block">•</div>
                    <div>Age: <span className="font-SVN-Gilroy-Bold ml-1">{props.age} Months</span></div>
                </div>
                <div className="text-base md:text-sm font-SVN-Gilroy-Bold text-black/90">{props.price} VND</div>
            </div>
        </a>
    );
}