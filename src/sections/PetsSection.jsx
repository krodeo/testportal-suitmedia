import Wrapper from "../components/Wrapper";
import { IconArrowLeft } from "../components/Icons";
import CardPet from "../components/CardPet";

import pomeranianWhite from '../assets/images/pets/pomeranian-white.jpg';
import poodleTinyYellow from '../assets/images/pets/poodle-tiny-yellow.jpg';
import poodleTinySepia from '../assets/images/pets/poodle-tiny-sepia.jpg';
import alaskanMalamuteGrey from '../assets/images/pets/alaskan-malamute-grey.jpg';
import pembrokeCorgiCream from '../assets/images/pets/pembroke-corgi-cream.jpg';
import pembrokeCorgiTricolor from '../assets/images/pets/pembroke-corgi-tricolor.jpg';
import pomeranianWhite2 from '../assets/images/pets/pomeranian-white-2.jpg';
import poodleTinyDairyCow from '../assets/images/pets/poodle-tiny-dairy-cow.jpg';


export default function PetsSection() {
    const dogs = [
        { id: 'MO231', name: 'Pomeranian White', gene: 'Male', age: '02', price: '6.900.000', image: pomeranianWhite },
        { id: 'MO502', name: 'Poodle Tiny Yellow', gene: 'Female', age: '02', price: '3.900.000', image: poodleTinyYellow },
        { id: 'MO102', name: 'Poodle Tiny Sepia', gene: 'Male', age: '02', price: '4.000.000', image: poodleTinySepia },
        { id: 'MO512', name: 'Alaskan Malamute Grey', gene: 'Male', age: '02', price: '8.900.000', image: alaskanMalamuteGrey },
        { id: 'MO231', name: 'Pembroke Corgi Cream', gene: 'Male', age: '02', price: '7.900.000', image: pembrokeCorgiCream },
        { id: 'MO502', name: 'Pembroke Corgi Tricolor', gene: 'Female', age: '02', price: '9.000.000', image: pembrokeCorgiTricolor },
        { id: 'MO231', name: 'Pomeranian White', gene: 'Female', age: '02', price: '6.500.000', image: pomeranianWhite2 },
        { id: 'MO512', name: 'Poodle Tiny Dairy Cow', gene: 'Male', age: '02', price: '5.000.000', image: poodleTinyDairyCow }
    ]

    return (
        <Wrapper className="py-8 lg:py-16">
            <div className="flex items-end mb-5 lg:mb-10">
                <div>
                    <div className="text-sm md:text-base font-bold">Whats new?</div>
                    <h2 className="text-xl md:text-2xl font-bold font-SVN-Gilroy-Bold text-theme">Take a look at some of our pets</h2>
                </div>
                <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button hidden md:inline-flex ml-auto" aria-label="view more pets button">View more <IconArrowLeft className="ml-3" /></a>
            </div>
            <div className="grid grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-3 md:gap-4">
                {dogs.map((dog, index) => <CardPet key={index} {...dog} />)}
            </div>
            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button w-full md:hidden mx-auto mt-6" aria-label="view more pets button">View more <IconArrowLeft className="ml-3" /></a>
        </Wrapper>
    );
}