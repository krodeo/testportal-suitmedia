import Wrapper from "../components/Wrapper";
import { IconArrowLeft } from "../components/Icons";
import ShebaImage from "../assets/images/sellers/sheba.jpg";
import WhiskasImage from "../assets/images/sellers/whiskas.jpg";
import BakersImage from "../assets/images/sellers/bakers.jpg";
import FelixImage from "../assets/images/sellers/felix.jpg";
import GoodboyImage from "../assets/images/sellers/good-boy.jpg";
import ButchersImage from "../assets/images/sellers/butchers.jpg";
import PedirgeeImage from "../assets/images/sellers/pedigree.jpg";


export default function SellersSection() {
    return (
        <Wrapper className="hidden md:block py-5 lg:py-16">
            <div className="flex items-end mb-5 lg:mb-10">
                <div>
                    <div className="inline-block text-base font-bold mr-2">Proud to be part of</div>
                    <h2 className="inline-block text-2xl font-bold font-SVN-Gilroy-Bold text-theme">Pet Sellers</h2>
                </div>
                <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button ml-auto">View all our sellers <IconArrowLeft className="ml-3" /></a>
            </div>
            <div className="grid grid-cols-5 lg:grid-cols-7 lg:gap-x-3">
                <img className=" w-full cursor-pointer hover:scale-125 transition-transform" src={ShebaImage} alt="Sheba" />
                <img className=" w-full cursor-pointer hover:scale-125 transition-transform" src={WhiskasImage} alt="Whiskas" />
                <img className=" w-full cursor-pointer hover:scale-125 transition-transform" src={BakersImage} alt="Baker's" />
                <img className=" w-full cursor-pointer hover:scale-125 transition-transform" src={FelixImage} alt="Felix" />
                <img className=" w-full cursor-pointer hover:scale-125 transition-transform" src={GoodboyImage} alt="Good Boy" />
                <img className=" w-full cursor-pointer hover:scale-125 transition-transform" src={ButchersImage} alt="Butcher" />
                <img className=" w-full cursor-pointer hover:scale-125 transition-transform" src={PedirgeeImage} alt="Pedrigee" />
            </div>
        </Wrapper>
    );
}