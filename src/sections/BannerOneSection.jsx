import Wrapper from "../components/Wrapper";
import BannerBackground from "../assets/images/misc/banner-one-bg.png";
import BannerBackgroundMobile from "../assets/images/misc/banner-one-bg-mobile.png";
import { IconPlay } from "../components/Icons";
import LadyImage from "../assets/images/misc/lady-holding-a-dog.png"

export default function BannerOneSection() {
    return (
        <Wrapper>
            <div className="grid lg:grid-cols-2 items-center gap-y-5 px-3 lg:px-0 pt-5 sm:pt-10 lg:pt-0 rounded-2xl lg:h-[378px] overflow-hidden bg-theme/20 relative">
                <div className="absolute w-full h-full z-0">
                    <img src={BannerBackground} className="hidden md:block w-full h-full object-cover object-right" alt="" />
                    <img src={BannerBackgroundMobile} className="md:hidden w-full h-full object-cover" alt="" />
                </div>
                <div className="order-2 lg:order-1 self-end mx-auto lg:ml-4 lg:pl-16 z-10">
                    <img src={LadyImage} className="w-full h-full object-contain" alt="lady-holding-a-dog" />
                </div>
                <div className="order-1 lg:order-2 flex flex-col items-center lg:items-end xl:pl-32 lg:pr-16 text-center lg:text-right z-10">
                    <h1 className="text-4xl md:text-5xl xl:text-[52px] font-SVN-Gilroy-Bold text-theme">One more friend</h1>
                    <h2 className="text-2xl md:text-3xl xl:text-4xl font-SVN-Gilroy-Bold text-theme mb-4">Thousands more fun!</h2>
                    <p className="text-xs mb-8 text-theme-darker max-w-lg">Having a pet means you have more joy, a new friend, a happy person who will always be with you to have fun. We have 200+ different pets that can meet your needs!</p>
                    <div className="flex items-center gap-x-4">
                        <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button" aria-label="view intro button">View Intro<IconPlay className="ml-2" /></a>
                        <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button bg-theme text-white hover:bg-slate-900" aria-label="explore more button">Explore Now</a>
                    </div>
                </div>
            </div>
        </Wrapper>
    )
}