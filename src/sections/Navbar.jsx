import Wrapper from "../components/Wrapper";
import Logo from "../assets/icons/logo.svg";
import { useWindowScroll } from "../hooks/CustomHooks";
import CurrencyPicker from "../components/CurrencyPicker";
import { IconArrowLeft, IconSearch, IconBurger, IconX } from "../components/Icons";
import { useState } from "react";

export default function Navbar() {
    const isScrolled = useWindowScroll();
    const [isSearchPanelOpen, setIsSearchPanelOpen] = useState(false);
    const [isSidebarOpen, setIsSidebarOpen] = useState(false);

    const handleSearchClick = (e) => {
        e.preventDefault();
        setIsSearchPanelOpen(!isSearchPanelOpen);
    }

    const handleSidebarClick = (e) => {
        e.preventDefault();
        setIsSidebarOpen(!isSidebarOpen);
    }

    return (
        <>
            <div className={`fixed top-0 left-0 w-full z-40 transition-colors duration-200 ${isScrolled && 'bg-white'}`}>
                {/* Main Nav */}
                <Wrapper className="px-4 xl:px-0">
                    <div className="flex items-center justify-between lg:justify-normal gap-x-3 py-4 lg:py-5">
                        <a href="" className="lg:hidden" onClick={handleSidebarClick} aria-label="Open menu button"><IconBurger /></a>
                        <a href="https://testportal-suitmedia-yaqin.netlify.app/"><img src={Logo} className="h-8 lg:h-10" alt="Monito" /></a>
                        <a href="" onClick={handleSearchClick} className="lg:hidden" aria-label="open search button"><IconSearch className="scale-125" /></a>
                        <nav className="grow hidden lg:flex justify-between gap-x-3 mx-5">
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="text-theme font-SVN-Gilroy-Bold" aria-label="Navigation Home">Home</a>
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="text-theme font-SVN-Gilroy-Bold" aria-label="Navigation Category">Category</a>
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="text-theme font-SVN-Gilroy-Bold" aria-label="Navigation About">About</a>
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="text-theme font-SVN-Gilroy-Bold" aria-label="Navigation Contact">Contact</a>
                        </nav>
                        <div className="hidden lg:block relative">
                            <IconSearch className="absolute left-4 top-1/2 -translate-y-1/2 text-gray-600" />
                            <input type="text" className={`xl:w-72 pl-12 pr-4 py-2.5 border border-transparent rounded-full focus:outline-none focus:border-sky-500 text-sm transition-colors duration-200 ${isScrolled && 'bg-theme/10'}`} placeholder="Search something here!" />
                        </div>
                        <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button hidden lg:inline-flex bg-theme text-white hover:bg-zinc-800" aria-label="Join the community button">Join the community</a>
                        <CurrencyPicker className="hidden lg:block ml-3" />
                    </div>
                </Wrapper>

                {/* Search Nav On Mobile */}
                {(isSearchPanelOpen && !isSidebarOpen) && (
                    <Wrapper className="!absolute top-0 right-0 left-0 flex items-center bg-white px-5 lg:hidden">
                        <IconSearch className="text-gray-600" />
                        <input type="text" className="w-full py-5 px-4 border border-transparent rounded-full focus:outline-none text-sm" placeholder="Search something here!" />
                        <a href="" onClick={handleSearchClick} aria-label="close search button"><IconX className="text-red-600" /></a>
                    </Wrapper>
                )}

                {/* Nav Mobile Ver */}
                {(isSidebarOpen && !isSearchPanelOpen) && (
                    <Wrapper className="!absolute top-0 left-0 right-0 bg-white lg:hidden">
                        <div className="w-full h-screen max-h-screen px-4 py-9 overflow-hidden">
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="text-red-500 absolute top-3 right-3" onClick={handleSidebarClick}><IconX /></a>
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="block text-theme text-lg font-SVN-Gilroy-Bold mb-4" aria-label="navigation home">Home</a>
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="block text-theme text-lg font-SVN-Gilroy-Bold mb-4" aria-label="navigation category">Category</a>
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="block text-theme text-lg font-SVN-Gilroy-Bold mb-4" aria-label="navigation about">About</a>
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="block text-theme text-lg font-SVN-Gilroy-Bold mb-6" aria-label="navigation contact">Contact</a>
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button flex bg-theme text-white hover:bg-zinc-800" aria-label="join the community button">Join the community</a>
                        </div>
                    </Wrapper>
                )}
            </div>
            <button onClick={() => window.scrollTo({ top: 0, behavior: 'smooth' })} className={`button rounded-lg fixed bottom-4 right-4 px-4 py-3 md:py-2 md:px-6 bg-theme text-white opacity-0 transition-opacity z-40 ${isScrolled && 'opacity-100'}`} role="button" aria-label="back to top button"><span className="mr-3 hidden md:inline">Back to top</span><IconArrowLeft className="-rotate-90" /></button>
        </>
    );
}