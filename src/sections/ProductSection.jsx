import Wrapper from "../components/Wrapper";
import { IconArrowLeft } from "../components/Icons";
import CardProduct from "../components/CardProduct";

import productThumbnail1 from '../assets/images/products/reflex-plus-adult-cat-food-salmon.jpg';
import productThumbnail2 from '../assets/images/products/reflex-plus-adult-cat-food-salmon-2.jpg';
import productThumbnail3 from '../assets/images/products/cat-scratching-ball-toy-kitten-sisal-rope-ball.jpg';
import productThumbnail4 from '../assets/images/products/cute-pet-cat-warm-nest.jpg';
import productThumbnail5 from '../assets/images/products/naturvet-dogs-omega-gold-plus-salmon-oil.jpg';
import productThumbnail6 from '../assets/images/products/costumes-fashion-pet-clother-cowboy-rider.jpg';
import productThumbnail7 from '../assets/images/products/costumes-chicken-drumsti-ck-headband.jpg';
import productThumbnail8 from '../assets/images/products/plush-pet-toy.jpg';


export default function ProductSection() {
    const products = [
        { name: 'Reflex Plus Adult Cat Food Salmon', product: 'Dog Food', size: '385gr', price: '140.000', bonus: 'Free Toy & Free Shaker', image: productThumbnail1 },
        { name: 'Reflex Plus Adult Cat Food Salmon', product: 'Cat Food', size: '1.5kg', price: '165.000 VND', bonus: 'Free Toy & Free Shaker', image: productThumbnail2 },
        { name: 'Cat scratching ball toy kitten sisal rope ball', product: 'Toy', size: '', price: '1.100.000', bonus: 'Free Cat Food', image: productThumbnail3 },
        { name: 'cute pet cat warm nest', product: 'Toy', size: '', price: '410.000', bonus: 'Free Cat Food', image: productThumbnail4 },
        { name: 'NaturVet Dogs - Omega-Gold Plus Salmon Oil', product: 'Dog Food', size: '385gr', price: '350.000', bonus: 'Free Toy & Free Shaker', image: productThumbnail5 },
        { name: 'Costumes Fashion Pet Clother Cowboy Rider', product: 'Costume', size: '1.5kg', price: '500.000', bonus: 'Free Toy & Free Shaker', image: productThumbnail6 },
        { name: 'Costumes Chicken Drumsti ck Headband', product: 'Costume', size: '', price: '400.000', bonus: 'Free Cat Food', image: productThumbnail7 },
        { name: 'Plush Pet Toy', product: 'Toy', size: '', price: '250.000', bonus: 'Free Food & Shaker', image: productThumbnail8 },
    ]

    return (
        <Wrapper className="hidden md:block py-5 lg:py-16">
            <div className="flex items-end mb-5 lg:mb-10">
                <div>
                    <div className="text-sm md:text-base font-bold">Hard to choose right products for your pets?</div>
                    <h2 className="text-xl md:text-2xl font-bold font-SVN-Gilroy-Bold text-theme">Our Products</h2>
                </div>
                <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button ml-auto" aria-label="view more products button">View more <IconArrowLeft className="ml-3" /></a>
            </div>
            <div className="grid grid-cols-3 xl:grid-cols-4 gap-3 md:gap-4">
                {products.map((product, index) => <CardProduct key={index} {...product} />)}
            </div>
        </Wrapper>
    );
}