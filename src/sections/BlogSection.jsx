import Wrapper from "../components/Wrapper";
import { IconArrowLeft } from "../components/Icons";
import CardBlog from "../components/CardBlog";

import thumbnail1 from '../assets/images/articles/thumbnail1.jpg';
import thumbnail2 from '../assets/images/articles/thumbnail2.jpg';
import thumbnail3 from '../assets/images/articles/thumbnail3.jpg';


export default function BlogSection() {
    const articles = [
        { title: 'What is a Pomeranian? How to Identify Pomeranian Dogs', category: 'Pet Knowledge', summary: "The Pomeranian, also known as the Pomeranian (Pom dog), is always in the top of the cutest pets. Not only that, the small, lovely, smart, friendly, and skillful circus dog breed.", thumbnail: thumbnail1 },
        { title: 'Dog Diet You Need To Know', category: 'Pet Knowledge', summary: "Dividing a dog's diet may seem simple at first, but there are some rules you should know so that your dog can easily absorb the nutrients in the diet. For those who are just starting to raise dogs, especially newborn puppies with relatively weak resistance.", thumbnail: thumbnail2 },
        { title: 'Why Dogs Bite and Destroy Furniture and How to Prevent It Effectively', category: 'Pet Knowledge', summary: "Dog bites are common during development. However, no one wants to see their furniture or important items being bitten by a dog.", thumbnail: thumbnail3 }
    ]

    return (
        <Wrapper className="py-8 lg:py-16">
            <div className="flex items-end mb-5 lg:mb-10">
                <div>
                    <div className="text-sm md:text-base font-bold">You already know ?</div>
                    <h2 className="text-xl md:text-2xl font-bold font-SVN-Gilroy-Bold text-theme">Useful Pet Knowledge</h2>
                </div>
                <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button hidden md:inline-flex ml-auto" aria-label="view more article button">View more <IconArrowLeft className="ml-3" /></a>
            </div>
            <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-5">
                {articles.map((article, index) => <CardBlog key={index} {...article} />)}
            </div>
            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button w-full md:hidden mx-auto mt-6" aria-label="view more article button">View more <IconArrowLeft className="ml-3" /></a>
        </Wrapper>
    );
}