import Wrapper from "../components/Wrapper";
import { IconPlay } from "../components/Icons";
import ImageHero from "../assets/images/misc/holding-dog.png";
import HeroBackground from "../assets/images/misc/hero-bg.png";
import HeroBackgroundMobile from "../assets/images/misc/hero-bg-mobile.png";

export default function HeroSection() {
    return (
        <div className="lg:h-[695px] bg-orange-100 rounded-b-3xl relative overflow-hidden">
            <div className="absolute w-full h-full z-0">
                <img src={HeroBackground} className="hidden lg:block w-full h-full object-cover object-left" alt="" />
                <img src={HeroBackgroundMobile} className="lg:hidden w-full h-full object-cover object-top" alt="" />
            </div>
            <Wrapper className="h-full">
                <div className="grid lg:grid-cols-2 items-center h-full max-w-lg lg:max-w-none mx-auto">
                    <div className="pb-5 pt-28">
                        <h1 className="font-SVN-Gilroy-Bold text-theme text-[44px] lg:text-6xl">One More Friend</h1>
                        <h2 className="font-SVN-Gilroy-Bold text-theme text-[28px] lg:text-5xl mb-8">Thousands More Fun!</h2>
                        <p className="text-xs lg:text-base max-w-lg mb-9">Having a pet means you have more joy, a new friend, a happy person who will always be with you to have fun. We have 200+ different pets that can meet your needs!</p>
                        <div className="flex items-center gap-x-4">
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button" aria-label="view intro button">View Intro<IconPlay className="ml-2" /></a>
                            <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button bg-theme text-white hover:bg-slate-900" aria-label="explore more button">Explore Now</a>
                        </div>
                    </div>
                    <div className="self-end w-full">
                        <img src={ImageHero} className="h-full" alt="holding-dog" />
                    </div>
                </div>
            </Wrapper>
        </div>
    );
}