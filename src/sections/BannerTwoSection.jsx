import Wrapper from "../components/Wrapper";
import BannerBackground from "../assets/images/misc/banner-two-bg.png";
import { IconPlay, IconPaw } from "../components/Icons";
import HoldingImage from "../assets/images/misc/holding-paw.png"

export default function BannerTwoSection() {
    const STYLE = {
        backgroundImage: `url(${BannerBackground})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'auto 100%',
        backgroundPosition: 'left center',
        height: '378px',
    }
    return (
        <Wrapper className="hidden md:block">
            <div className="grid grid-cols-2 items-center rounded-2xl" style={STYLE}>
                <div className="flex flex-col px-6 pr-0 lg:px-16">
                    <div className="flex items-center">
                        <h1 className="text-[52px] font-SVN-Gilroy-Bold font-extrabold text-theme mr-3">Adoption</h1>
                        <IconPaw />
                    </div>
                    <h2 className="text-4xl font-SVN-Gilroy-Bold text-theme mb-4">We Need Help. So Do They.</h2>
                    <p className="text-xs mb-8 text-theme-darker">Adopt a pet and give it a home,<br></br>it will be love you back unconditionally.</p>
                    <div className="flex items-center gap-x-4">
                        <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button bg-theme text-white hover:bg-slate-900" aria-label="explore more button">Explore Now</a>
                        <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="button" aria-label="view intro button">View Intro<IconPlay className="ml-2" /></a>
                    </div>
                </div>
                <div className="ml-4 h-full">
                    <img src={HoldingImage} className="h-full w-full object-cover" alt="lady-holding-a-dog" />
                </div>
            </div>
        </Wrapper>
    )
}