import Wrapper from "../components/Wrapper";
import { IconFacebook, IconTwitter, IconInstagram, IconYoutube } from "../components/Icons";
import Logo from "../assets/icons/logo.svg"
import SubscribePanel from "../components/SubscribePanel";

export default function FooterSection() {
    return (
        <footer className="bg-orange-100/90 rounded-t-3xl">
            <Wrapper className="pt-12 lg:pt-20">
                <SubscribePanel />
                <div className="flex flex-col md:flex-row items-center justify-between gap-y-7 py-10">
                    <ul className="flex items-center gap-x-10 md:gap-x-16">
                        <li><a href="https://testportal-suitmedia-yaqin.netlify.app/" className="font-bold hover:opacity-70" aria-label="navigation home">Home</a></li>
                        <li><a href="https://testportal-suitmedia-yaqin.netlify.app/" className="font-bold hover:opacity-70" aria-label="navigation category">Category</a></li>
                        <li><a href="https://testportal-suitmedia-yaqin.netlify.app/" className="font-bold hover:opacity-70" aria-label="navigation about">About</a></li>
                        <li><a href="https://testportal-suitmedia-yaqin.netlify.app/" className="font-bold hover:opacity-70" aria-label="navigation contact">Contact</a></li>
                    </ul>
                    <ul className="flex items-center gap-x-10">
                        <li><a href="https://testportal-suitmedia-yaqin.netlify.app/" className="hover:opacity-70" aria-label="go to facebook button"><IconFacebook /></a></li>
                        <li><a href="https://testportal-suitmedia-yaqin.netlify.app/" className="hover:opacity-70" aria-label="go to twitter button"><IconTwitter /></a></li>
                        <li><a href="https://testportal-suitmedia-yaqin.netlify.app/" className="hover:opacity-70" aria-label="go to instagram button"><IconInstagram /></a></li>
                        <li><a href="https://testportal-suitmedia-yaqin.netlify.app/" className="hover:opacity-70" aria-label="go to youtube button"><IconYoutube /></a></li>
                    </ul>
                </div>
                <hr className="border-blue-800/30" />
                <div className="flex flex-col md:flex-row items-center justify-between gap-y-2 py-10">
                    <div className="order-3 md:order-1 text-xs md:text-sm text-theme/70 font-bold">© {new Date().getFullYear()} Monito. All rights reserved.</div>
                    <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="order-1 md:order-2 mb-7 md:mb-0" aria-label="go to home button"><img src={Logo} alt="" /></a>
                    <div className="order-2 md:order-3 flex items-center gap-6">
                        <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="text-sm text-theme/70 font-bold hover:opacity-70" aria-label="term of service">Terms of Service</a>
                        <a href="https://testportal-suitmedia-yaqin.netlify.app/" className="text-sm text-theme/70 font-bold hover:opacity-70" aria-label="privacy policy">Privacy Policy</a>
                    </div>
                </div>
            </Wrapper>
        </footer>
    );
}