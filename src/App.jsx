import BannerOneSection from './sections/BannerOneSection'
import BannerTwoSection from './sections/BannerTwoSection'
import BlogSection from './sections/BlogSection'
import FooterSection from './sections/FooterSection'
import HeroSection from './sections/HeroSection'
import Navbar from './sections/Navbar'
import PetsSection from './sections/PetsSection'
import ProductSection from './sections/ProductSection'
import SellersSection from './sections/SellersSection'

function App() {
    return (
        <>
            <Navbar />
            <HeroSection />
            <PetsSection />
            <BannerOneSection />
            <ProductSection />
            <SellersSection />
            <BannerTwoSection />
            <BlogSection />
            <FooterSection />
        </>
    )
}

export default App
